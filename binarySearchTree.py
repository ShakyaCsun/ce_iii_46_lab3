class BST:
	def __init__(self):
		self._root = None
		self._size = 0

	def size(self):
		return self._size

	class _BSTNode:
		def __init__(self, key, value):
			self.key = key
			self.value = value
			self.left = None
			self.right = None
# (a) ADD a node to a BST
	def insert(self, key, value): # Returns False if key already exists in the BST, True if key is added to BST
		node = self._bstSearch(self._root, key)
		if node is not None:
			node.value = value
			return False

		else:
			self._root = self._bstInsert(self._root, key, value)
			self._size += 1
			return True
		
	def _bstInsert(self, subtree, key, value):
		if subtree is None:
			subtree = self._BSTNode(key, value)
		elif key < subtree.key:
			subtree.left = self._bstInsert(subtree.left, key, value)
		elif key > subtree.key:
			subtree.right = self._bstInsert(subtree.right, key, value)
		return subtree

# (b) Search BST for the requested key
	def bstSearch(self, key):
		return self._bstSearch(self._root, key) is not None #Returns True if requested key is found and False if not found

	def _bstSearch(self, subtree, target):
		if subtree is None:
			return None
		elif target < subtree.key:
			return self._bstSearch(subtree.left, target)
		elif target > subtree.key:
			return self._bstSearch(subtree.right, target)
		else:
			return subtree

# (c) Find the smallest key
	def getMinimum(self):
		return self._getMinimum(self._root)

	def _getMinimum(self, subtree):
		if subtree is None:
			return None
		elif subtree.left is None:
			return subtree.key 	# this returns the smallest key
		else:
			return self._getMinimum(subtree.left)

# (d) Find the largest key
	def getMaximum(self):
		return self._getMaximum(self._root)
		
	def _getMaximum(self, subtree):
		if subtree is None:
			return None
		elif subtree.right is None:
			return subtree.key 	# this returns the largest key
		else:
			return self._getMaximum(subtree.right)

# (e) Delete a key from a BST
	def delete(self, key):       # Returns True if key is found and deleted, False if requested key doesn't exist in BST
		if(self.bstSearch(key)): # Checking if the key exists in BST or not
			self._root = self._bstDelete(self._root, key)
			self._size -= 1
			return True
		return False

	def _bstDelete(self, subtree, target):
		if subtree is None:
			return subtree
		elif target < subtree.key:
			subtree.left = self._bstDelete(subtree.left, target)
			return subtree
		elif target > subtree.key:
			subtree.right = self._bstDelete(subtree.right, target)
			return subtree
		else:
			if subtree.left is None and subtree.right is None:
				return None
			elif subtree.left is None or subtree.right is None:
				if subtree.left is not None:
					return subtree.left
				else:
					return subtree.right
			else:
				successor = self._getMinimum(subtree.right)
				subtree.key = successor.key
				subtree.value = successor.value
				subtree.right = self._bstDelete(subtree.right, successor.key)
				return subtree
	
# (f) Inorder traversal
	def inorderTreeWalk(self):
		nodes = []
		self._inorderTreeWalk(self._root, nodes)
		return nodes

	def _inorderTreeWalk(self, subtree, nodes):
		if(subtree):
			self._inorderTreeWalk(subtree.left, nodes)
			nodes.append(subtree.key)
			self._inorderTreeWalk(subtree.right, nodes)

# (g) Preorder traversal
	def preorderTreeWalk(self):
		nodes = []
		self._preorderTreeWalk(self._root, nodes)
		return nodes

	def _preorderTreeWalk(self, subtree, nodes):
		if(subtree):
			nodes.append(subtree.key)
			self._preorderTreeWalk(subtree.left, nodes)
			self._preorderTreeWalk(subtree.right, nodes)

# (h) Postorder traversal
	def postorderTreeWalk(self):
		nodes = []
		self._postorderTreeWalk(self._root, nodes)
		return nodes

	def _postorderTreeWalk(self, subtree, nodes):
		if(subtree):
			self._postorderTreeWalk(subtree.left, nodes)
			self._postorderTreeWalk(subtree.right, nodes)
			nodes.append(subtree.key)
