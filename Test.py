import unittest
from binarySearchTree import BST

class BSTTestCase(unittest.TestCase):
	def testBinarySearch(self):
		binaryTree = BST()
# Test for Add a Node
		self.assertEqual(binaryTree.insert(15, "Value 1"), True) #Inserting 15 returns True
		self.assertEqual(binaryTree.insert(15, "Value 1"), False) #Trying to insert 15 again returns False
		self.assertEqual(binaryTree.size(), 1) #Size changes to 1 after inserting 15

		binaryTree.insert(7, "Value 2")
		self.assertEqual(binaryTree.size(), 2)

		binaryTree.insert(23, "Value 3")
		self.assertEqual(binaryTree.size(), 3)
# Tests for Inorder, Preorder and Postorder Traversal after inserting 15, 7 and 23
		self.assertListEqual(binaryTree.inorderTreeWalk(), [7, 15, 23])
		self.assertListEqual(binaryTree.preorderTreeWalk(), [15, 7, 23])
		self.assertListEqual(binaryTree.postorderTreeWalk(), [7, 23, 15])
# Tests for finding smallest and largest key
		self.assertEqual(binaryTree.getMinimum(), 7)
		self.assertEqual(binaryTree.getMaximum(), 23)
# Tests for searching in binary search tree
		self.assertEqual(binaryTree.bstSearch(15), True)
		self.assertEqual(binaryTree.bstSearch(11), False)

		binaryTree.insert(40, "Value 3")
		self.assertEqual(binaryTree.size(), 4)
# Tests for deleting a node
		self.assertEqual(binaryTree.delete(40), True)
		self.assertEqual(binaryTree.size(),3)
		self.assertEqual(binaryTree.delete(70), False)
		self.assertEqual(binaryTree.size(),3)
if __name__  == '__main__':
	unittest.main()
